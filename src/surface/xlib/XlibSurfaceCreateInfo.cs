﻿using System;
using System.Runtime.InteropServices;

// ReSharper disable PrivateFieldCanBeConvertedToLocalVariable

namespace oovulkan.surface.xlib
{
    [Flags]
    internal enum XlibSurfaceCreateFlags : int
    {
        None = 0
    }

    [StructLayout(LayoutKind.Sequential)]
    internal unsafe struct XlibSurfaceCreateInfo
    {
        private readonly StructureType Type;
        private readonly void* Next;
        private readonly XlibSurfaceCreateFlags Flags;
        public readonly XlibDisplayHandle Display;
        public readonly XlibVisualHandle Visual;

        public XlibSurfaceCreateInfo(XlibDisplayHandle display, XlibVisualHandle visual)
        {
            Type = StructureType.XlibSurfaceCreateInfoKhr;
            Next = null;
            Flags = XlibSurfaceCreateFlags.None;
            Display = display;
            Visual = visual;
        }
    }
}