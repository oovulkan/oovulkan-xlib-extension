﻿using System.Runtime.InteropServices;
using oovulkan.device;
using oovulkan.instance;

// ReSharper disable InconsistentNaming
#pragma warning disable 649

namespace oovulkan.surface.xlib
{
    public unsafe class XlibSurface : Surface
    {
        public readonly XlibDisplayHandle Display;
        public readonly XlibVisualHandle Visual;

        internal XlibSurface(Device device, XlibDisplayHandle display, XlibVisualHandle visual)
            : base(device)
        {
            Display = display;
            Visual = visual;
            Create();
        }

        private void Create()
        {
            #region Validation
            FilterQueueFamilies(family => family.SupportsXlib(Display, Visual));
            #endregion

            var info = new XlibSurfaceCreateInfo(Display, Visual);
            using(Device.WeakLock)
            {
                SurfaceHandle handle;
                using(Device.PhysicalDevice.Instance.WeakLock)
                using(Device.Allocator?.WeakLock)
                    vkCreateXlibSurfaceKHR(Device.PhysicalDevice.Instance, &info, Device.Allocator, out handle).Throw();
                Handle = handle;
                Device.Add(this);
            }
        }

        //[VulkanInterop.InstanceFunction]
        //private readonly VkCreateXlibSurfaceKHR vkCreateXlibSurfaceKHR;

        //private delegate Result VkCreateXlibSurfaceKHR(InstanceHandle instance, XlibSurfaceCreateInfo* info, AllocationCallbacks* allocator, out SurfaceHandle surface);

        [DllImport("libvulkan.so")]
        private static extern Result vkCreateXlibSurfaceKHR(InstanceHandle instance, XlibSurfaceCreateInfo* info, AllocationCallbacks* allocator, out SurfaceHandle surface);
    }

    public static class XlibSurfaceHelper
    {
        public static XlibSurface GetXlibSurface(this Device device, XlibDisplayHandle display, XlibVisualHandle visual)
            => new XlibSurface(device, display, visual);
    }
}